require('dotenv').config();
const express    = require('express');
const bodyParser = require('body-parser');
const routes     = require('./routes/api');

const app = express();

app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

app.use(bodyParser.json());

app.use('/api', routes);

app.use((err, req, res, next) => {
  console.log(err);
  next();
});

app.listen(process.env.PORT, () => {
  console.log(`Server running on port ${process.env.PORT}`)
});