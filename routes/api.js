const express = require('express');
const controller = require('../controller/controller');
const { formValidation } = require('../validation/form.validation');

const router = express.Router();

router.post('/pendaftaran' , formValidation, controller.signUp);

module.exports = router
