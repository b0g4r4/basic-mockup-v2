# Form Validation
# How to Setup 
After clone this repository, cd inside the folder, create .env file with PORT=YOUR_PREFERED_PORT (Example PORT = 5000)
then run
```
npm install
node index.js
```
Then you can start testing in Postman

# How to Use

## Localhost

### Success
![alt text](https://gitlab.com/telkomsel-softdev/andre-aginsa/validation-form/-/raw/master/results/result_local_success.png?raw=true)

### Error
![alt text](https://gitlab.com/telkomsel-softdev/andre-aginsa/validation-form/-/raw/master/results/result_local_error1.png?raw=true)
![alt text](https://gitlab.com/telkomsel-softdev/andre-aginsa/validation-form/-/raw/master/results/result_local_error2.png?raw=true)

## NGROK

### Success
![alt text](https://gitlab.com/telkomsel-softdev/andre-aginsa/validation-form/-/raw/master/results/result_ngrok_success.png?raw=true)

### Error
![alt text](https://gitlab.com/telkomsel-softdev/andre-aginsa/validation-form/-/raw/master/results/result_ngrok_error1.png?raw=true)
![alt text](https://gitlab.com/telkomsel-softdev/andre-aginsa/validation-form/-/raw/master/results/result_ngrok_error2.png?raw=true)


