module.exports = {
  signUp: (req,res) => {
    if (req.error.length != 0) {
      res.status(422).json({ message: 'Data bermasalah', entity: 'Registration', state: 'invalidAttributes', error: req.error });
    } else {
      res.status(200).json({ message: 'Pendaftaran sukses', entity: 'Registration', state: 'success' });
    }
  },
};
