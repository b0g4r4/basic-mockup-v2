module.exports = {
  formValidation: (req, res, next) => {
    const error = [];
    for (const [key, value] of Object.entries(req.body)) {
      if (typeof(value) == 'string' && !value) {
        error.push({ key: key, msg: 'Tidak Boleh Kosong!' });
      } else if (typeof(value) == 'object' && value.length == 0)  {
        error.push({ key: key, msg: 'Tidak Boleh Kosong!' });
      } else {
        switch(key) {
          case 'nama':
            break;
          case 'ttl':
            break;
          case 'alamat':
            break;
          case 'agama':
            break;
          case 'agama':
            const religionList = ['islam','hindu','buddha','katolik','protestan','khonghucu'];
            if (religionList.indexOf(value.toLowerCase()) == -1) error.push({ key: key, msg: 'Agama tidak dalam daftar. Pilihan agama: islam, hindu, buddha, protestan, khonghucu' })
            break;
          case 'pendidikan':
            const educationList = ['TIDAK ADA','TK','SD','SMP','SMA','SMK','S1','S2','S3'];
            if (educationList.indexOf(value.toUpperCase()) == -1) error.push({ key: key, msg: 'Pendidikan tidak dalam daftar. Pilihan pendidikan: Tidak Ada, TK, SD, SMP, SMA, SMK, S1, S2, S3' })
            break;
          case 'golongan_darah':
            const bloodTypeList = ['A','B','O','AB'];
            if (bloodTypeList.indexOf(value.toUpperCase()) == -1) error.push({ key: key, msg: 'Golongan Darah tidak dalam daftar. Pilihan Golongan Darah: A, B, O, AB' })
            break;
          case 'riwayat_pekerjaan':
            value.forEach((object, index) => {
              for (const [key1, value1] of Object.entries(object)) {
                if (!value1)
                  error.push({ key: key+'.'+index+'.'+key1, msg: 'Tidak Boleh Kosong!' });
              }
            })
            break;
          case 'kontak_darurat':
            value.forEach((object, index) => {
              for (const [key1, value1] of Object.entries(object)) {
                if (!value1)
                  error.push({ key: key+'.'+index+'.'+key1, msg: 'Tidak Boleh Kosong!' });
              }
            })
            break;
          case 'gambar_ktp':
            const imgExtensionList = ['jpeg','jpg','png']
            const base64 = value.split(',')[0].split(';')[1];
            const dataImg = value.split(',')[0].split(';')[0].split('/')[0];
            const imgExt = value.split(',')[0].split(';')[0].split('/')[1];
            if (base64 != 'base64' || dataImg != 'data:image') {
              error.push({ key: key, msg: 'Gambar tidak valid!' })
            } else {
              if (imgExtensionList.indexOf(imgExt) == -1)
                error.push({ key: key, msg: 'Gambar yang bisa diterima hanya tipe: jpeg, jpg, png' })
            }  
            break;
        }
      }
    }
    req.error = error;
    next();
  }
};
